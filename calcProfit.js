function maxProfit(prices) {
    let profit = 0,
        exitFlag = true,
        addMas = [];
    return calculate(prices);

    function isIncrease(prices) {
        return prices.every((element, index, array) =>
            (index !== array.length - 1
                && array[index] < array[index + 1])
            || index === array.length - 1);
    }

    function isDecrease(prices) {
        return prices.every((element, index, array) =>
            (index !== array.length - 1
                && array[index] > array[index + 1])
            || index === array.length - 1);
    }

    function calculate(prices) {
        let addMas = [],
            isIncrease = isIncrease(prices);
        if (isIncrease) {
            while (prices.length > 1) {
                profit += (prices[prices.length - 1] + prices[0]);
                prices.splice(0, 1).splice(prices.length - 1, 1);
            }
        } else {
            while (prices.length > 1 || addMas > 1) {
                let indMax = prices.indexOf(Math.max(...prices));
                addMas = prices.splice(0, indMax);
                calculate(prices);
                calculate(addMas);
            }
        }
        return profit;
    }
}

module.exports = maxProfit;